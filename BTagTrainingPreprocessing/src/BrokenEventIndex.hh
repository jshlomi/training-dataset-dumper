#ifndef BROKEN_EVENT_INDEX_HH
#define BROKEN_EVENT_INDEX_HH

#include <H5Cpp.h>
#include <string>
#include <vector>

namespace H5 {
  class Group;
  class DataSet;
  class DataType;
}
namespace H5Utils {
  class WriterXd;
}

struct BrokenEvent
{
  unsigned long long entry_number;
  unsigned long long event_number;
  unsigned int jet_number;
  std::string file;
  std::string jet_collection;
  std::string what;
  // these guys are a bit of a hack to deal with the string types,
  // since hdf5 wants a pointer to the buffer rather than an actual
  // string
  const char* _file;
  const char* _jet_collection;
  const char* _what;
};

class BrokenEventWriter
{
public:
  BrokenEventWriter(H5::Group& output_file);
  ~BrokenEventWriter();
  BrokenEventWriter(BrokenEventWriter&) = delete;
  BrokenEventWriter& operator=(BrokenEventWriter&) = delete;
  void add(const BrokenEvent&);
  unsigned long long size() const;

private:
  void flush();
  unsigned long long m_offset;
  H5::DataSet m_dataset;
  H5::DataType m_type;
  std::vector<BrokenEvent> m_buffer;
};


#endif

#include "BTagROOTJetFiller.hh"

#include <cstddef>

#include "xAODJet/Jet.h"
#include "xAODEventInfo/EventInfo.h"

#include "TTree.h"

BTagROOTJetFiller::BTagROOTJetFiller(TTree &output_tree, const BTagJetWriterConfig &config) : m_config(config) {
  if (m_config.write_event_info) {
    output_tree.Branch("eventNumber", &m_eventNumber);
    output_tree.Branch("mcEventWeight", &m_mcEventWeight);
  }

  output_tree.Branch("pt", &m_pt);
  output_tree.Branch("eta", &m_eta);

  set_up_variable_group_branches(output_tree, m_jet_int_variables, config.truth_labels);
  set_up_variable_group_branches(output_tree, m_jet_float_variables, config.jet_float_variables);
  set_up_variable_group_branches(output_tree, m_btagging_char_variables, config.char_variables);
  set_up_variable_group_branches(output_tree, m_btagging_int_variables, config.int_variables);
  set_up_variable_group_branches(output_tree, m_btagging_int_variables, config.truth_labels);
  set_up_variable_group_branches(output_tree, m_btagging_int_as_float_variables, config.int_as_float_variables);
  set_up_variable_group_branches(output_tree, m_btagging_float_variables, config.float_variables);
  set_up_variable_group_branches(output_tree, m_btagging_double_variables, config.double_variables);
}

BTagROOTJetFiller::~BTagROOTJetFiller() {
  for (auto *variable : m_jet_int_variables) {
    delete variable;
  }
  for (auto *variable : m_jet_float_variables) {
    delete variable;
  }
  for (auto *variable : m_btagging_char_variables) {
    delete variable;
  }
  for (auto *variable : m_btagging_int_variables) {
    delete variable;
  }
  for (auto *variable : m_btagging_int_as_float_variables) {
    delete variable;
  }
  for (auto *variable : m_btagging_float_variables) {
    delete variable;
  }
  for (auto *variable : m_btagging_double_variables) {
    delete variable;
  }
}

template<typename T>
void BTagROOTJetFiller::set_up_variable_group_branches(TTree &tree, std::vector<T *> &variables, const std::vector<std::string> &variable_names) {
  for (const std::string &variable_name : variable_names) {
    variables.push_back(new T);
    std::string out_name;
    const auto &rename_map = m_config.variable_maps.rename;
    if (rename_map.count(variable_name)) {
      out_name = rename_map.at(variable_name);
    } else {
      out_name = variable_name;
    }
    tree.Branch(out_name.c_str(), variables.back());
  }
}

void BTagROOTJetFiller::fill(const xAOD::Jet &jet, const xAOD::EventInfo *const event_info) {
  const xAOD::BTagging &btagging = *jet.btagging();

  if (m_config.write_event_info) {
    m_eventNumber = event_info->eventNumber();
    m_mcEventWeight = event_info->mcEventWeight();
  }

  m_pt = jet.pt();
  m_eta = jet.eta();

  fill_jet_variable_group(jet, m_jet_int_variables, m_config.truth_labels);
  fill_jet_variable_group(jet, m_jet_float_variables, m_config.jet_float_variables);

  fill_btagging_variable_group<char>(btagging, m_btagging_char_variables, m_config.char_variables, (char) -1);
  fill_btagging_variable_group<int>(btagging, m_btagging_int_variables, m_config.int_variables, -1);
  fill_btagging_variable_group<int>(btagging, m_btagging_int_as_float_variables, m_config.int_as_float_variables, NAN);
  fill_btagging_variable_group<float>(btagging, m_btagging_float_variables, m_config.float_variables, NAN);
  fill_btagging_variable_group<double>(btagging, m_btagging_double_variables, m_config.double_variables, (double) NAN);
}

template<typename T>
void BTagROOTJetFiller::fill_jet_variable_group(const xAOD::Jet &jet, std::vector<T *> &variables, const std::vector<std::string> &variable_names) {
  for (std::size_t variable_index = 0; variable_index < variable_names.size(); ++variable_index) {
    const std::string &variable_name = variable_names.at(variable_index);
    *variables.at(variable_index) = jet.auxdata<T>(variable_name);
  }
}

template<typename I, typename O> // I is original (Input) variable type, O is Output variable type
void BTagROOTJetFiller::fill_btagging_variable_group(const xAOD::BTagging &btagging, std::vector<O *> &variables, const std::vector<std::string> &variable_names, const O default_value) {
  for (std::size_t variable_index = 0; variable_index < variable_names.size(); ++variable_index) {
    const std::string &variable_name = variable_names.at(variable_index);
    const auto &defaults_checks = m_config.variable_maps.replace_with_defaults_checks;
    if (defaults_checks.count(variable_name) && btagging.auxdata<char>(defaults_checks.at(variable_name))) {
      *variables.at(variable_index) = default_value;
    } else {
      *variables.at(variable_index) = btagging.auxdata<I>(variable_name);
    }
  }
}

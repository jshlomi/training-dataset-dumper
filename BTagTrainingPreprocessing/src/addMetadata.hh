#ifndef ADD_METADATA_HH
#define ADD_METADATA_HH

namespace H5 {
  class Group;
}

class Counts;

void addMetadata(H5::Group& grp, const Counts& counts);

#endif

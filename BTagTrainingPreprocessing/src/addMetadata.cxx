#include "addMetadata.hh"

#include "H5Cpp.h"
#include "BookKeeper.hh"

void addMetadata(H5::Group& grp, const Counts& counts){
  H5::CompType type(sizeof(Counts));
#define INSERT(THING, TYPE)                         \
  type.insertMember(#THING, offsetof(Counts, THING), TYPE)
  INSERT(nEventsProcessed, H5::PredType::NATIVE_LLONG);
  INSERT(sumOfWeights, H5::PredType::NATIVE_DOUBLE);
  INSERT(sumOfWeightsSquared, H5::PredType::NATIVE_DOUBLE);
  INSERT(nIncomplete, H5::PredType::NATIVE_INT);
  auto ds = grp.createDataSet("metadata", type, H5S_SCALAR);
  ds.write(&counts, type);
}


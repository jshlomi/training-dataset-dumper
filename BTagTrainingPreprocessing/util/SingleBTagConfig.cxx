#include "SingleBTagConfig.hh"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

namespace {
  bool boolinate(const boost::property_tree::ptree& pt,
                 const std::string key) {
    std::string val = pt.get<std::string>(key);
    if (val == "true") return true;
    if (val == "false") return false;
    throw std::logic_error("'" + key + "' should be 'true' or 'false'");
  }
  TrackSortOrder get_sort_order(const boost::property_tree::ptree& pt,
                                const std::string key) {
    std::string val = pt.get<std::string>(key);
    if (val == "abs_d0_significance") {
      return TrackSortOrder::ABS_D0_SIGNIFICANCE;
    }
    if (val == "abs_d0") {
      return TrackSortOrder::ABS_D0;
    }
    if (val == "d0_significance") {
      return TrackSortOrder::D0_SIGNIFICANCE;
    }
    throw std::logic_error("sort order '" + val + "' not recognized");
  }
}

SingleBTagConfig get_singlebtag_config(const std::string& config_file_name) {
  SingleBTagConfig config;

  boost::property_tree::ptree pt;
  boost::property_tree::read_json(config_file_name, pt);

  config.do_calibration = boolinate(pt, "do_calibration");
  config.jet_collection = pt.get<std::string>("jet_collection");

  if(config.do_calibration){
  config.jet_calib_file = pt.get<std::string>("jet_calib_file");
  config.cal_seq = pt.get<std::string>("cal_seq");
  config.cal_area = pt.get<std::string>("cal_area");
  config.jvt_cut = pt.get<float>("jvt_cut");
  }

  config.n_tracks_to_save = pt.get<size_t>("n_tracks_to_save");
  config.track_sort_order = get_sort_order(pt,"track_sort_order");

  // optional configuration
  config.iprnn_file_path = pt.get<std::string>("iprnn_file_path","");
  return config;
}

/// @file dump-test.cxx
/// @brief Test reading the main calo cluster and track particle container
///
/// This test simply reads in the static payload of the track particle
/// container of a primary xAOD. And checks how fast this can actually
/// be done.

#include "HbbOptions.hh"
#include "HbbConfig.hh"

// new way to do local things
#include "BTagTrackWriterConfig.hh"
#include "BTagJetWriterConfig.hh"
#include "BTagJetWriter.hh"
#include "BTagTrackWriter.hh"
#include "BookKeeper.hh"
#include "addMetadata.hh"
#include "TrackSelector.hh"
#include "BrokenEventIndex.hh"

#include "BTaggingWriterConfiguration.hh"

// System include(s):
#include <memory>
#include <string>

// ROOT include(s):
#include <TFile.h>
#include <TError.h>
#include <TLorentzVector.h>

// HDF includes
#include "H5Cpp.h"

// AnalysisBase tool include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/tools/ReturnCheck.h"
#include "BoostedJetTaggers/HbbTaggerDNN.h"
#include "BoostedJetTaggers/JSSWTopTaggerDNN.h"
#include "AsgTools/AnaToolHandle.h"
#include "FlavorTagDiscriminants/BTagTrackAugmenter.h"

// EDM include(s):
#include "JetCalibTools/JetCalibrationTool.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODTruth/TruthEventContainer.h"


// sort functions
bool by_d0(const xAOD::TrackParticle* t1,
           const xAOD::TrackParticle* t2) {
  static SG::AuxElement::ConstAccessor<float> d0("btag_ip_d0");
  return std::abs(d0(*t1)) > std::abs(d0(*t2));
}
bool pt_sort(const xAOD::Jet* sj1, const xAOD::Jet* sj2 ){
  return sj1->pt() > sj2->pt();
}
bool dR_sort(const xAOD::Jet* sj1, const xAOD::Jet* sj2){
  static SG::AuxElement::ConstAccessor<float> dR("dR_fatjet");
  return dR(*sj1) < dR(*sj2);
}

struct SubjetCollection {
  SubjetConfig cfg;
  typedef std::vector<ElementLink<xAOD::IParticleContainer> > ParticleLinks;
  SG::AuxElement::ConstAccessor<ParticleLinks> subjet_acc;
  std::vector<BTagJetWriter> jet_writers;
  std::vector<std::unique_ptr<BTagTrackWriter> > track_writers;
  SubjetCollection(SubjetConfig);
};
SubjetCollection::SubjetCollection(SubjetConfig init_cfg):
  cfg(init_cfg),
  subjet_acc(init_cfg.input_name)
{
}


const double GeV = 1000;

int main (int argc, char *argv[])
{
  const IOOpts opts = get_io_opts(argc, argv);
  const HbbConfig jobcfg = get_hbb_config(opts.config_file_name);
  // The name of the application:
  static const char *APP_NAME = "BTagTestDumper";

  TrackSelector track_selector;
  BTagTrackAugmenter track_augmenter;

  // Set up the environment:
  RETURN_CHECK( APP_NAME, xAOD::Init() );

  // Set up the event object:
  xAOD::TEvent event(xAOD::TEvent::kClassAccess);

  // Initialize JetCalibrationTool with release 21 recommendations
  JetCalibrationTool calib_tool("JetCalibrationTool");
  calib_tool.setProperty("JetCollection", jobcfg.jet_collection);
  calib_tool.setProperty("ConfigFile", jobcfg.jet_calib_file);
  calib_tool.setProperty("CalibSequence", jobcfg.cal_seq);
  calib_tool.setProperty("CalibArea", jobcfg.cal_area);
  calib_tool.setProperty("IsData", false);
  RETURN_CHECK( APP_NAME, calib_tool.initialize() );

  InDet::InDetTrackSelectionTool indettrackselectiontool("InDetTrackSelectionTool", "Loose");
  RETURN_CHECK( APP_NAME, indettrackselectiontool.initialize() );

  HbbTaggerDNN* tagger = nullptr;

  // maybe setup a neural network
  if (opts.nn_file.size() > 0) {
    tagger = new HbbTaggerDNN("Tagger");
    if (opts.verbose) tagger->setProperty("OutputLevel", MSG::DEBUG);
    tagger->setProperty("configurationFile", "BoostedJetTaggers/HbbTaggerDNN/MulticlassConfigJune2018.json");
    tagger->setProperty("neuralNetworkFile", opts.nn_file);
    RETURN_CHECK( APP_NAME, tagger->initialize());
  }

  JSSWTopTaggerDNN* top_tagger = nullptr;
  if (jobcfg.top_tag_config.size() > 0) {
    top_tagger = new JSSWTopTaggerDNN("TopTagger");
    top_tagger->setProperty("ConfigFile", jobcfg.top_tag_config);
    top_tagger->initialize();
  }
  SG::AuxElement::Decorator<float> dec_top_tagger("JSSTopScore");

  // define output file output files
  H5::H5File output(opts.out, H5F_ACC_TRUNC);

  // keep track of things that throw exceptions
  BrokenEventWriter broken_events(output);

  // set up fat jet writer
  BTagJetWriterConfig fat_cfg;
  fat_cfg.write_substructure_moments = true;
  fat_cfg.write_event_info = true;
  fat_cfg.jet_float_variables = cfg::FatJetFloats;
  if (jobcfg.save_xbb_score) {
    cfg::insert_into(fat_cfg.jet_float_variables, cfg::FatJetXbbScores);
  }
  if (top_tagger) fat_cfg.jet_float_variables.push_back("JSSTopScore");
  if (tagger) {
    for (const auto& name: tagger->decorationNames()) {
      fat_cfg.jet_float_variables.push_back(name);
    }
  }
  fat_cfg.parent_truth_labels = cfg::FatJetParentTruthLabels;
  fat_cfg.name = "fat_jet";
  BTagJetWriter fatjet_writer(output, fat_cfg);

  // set up vectors for accessors and subjet writers
  // subjet accessors
  typedef SG::AuxElement AE;
  typedef ElementLink<xAOD::JetContainer> JetLink;
  AE::ConstAccessor<JetLink> acc_parent("Parent");

  std::vector<SubjetCollection> subjet_collections;
  for(const auto& cfg: jobcfg.subjet_configs) {

    BTagJetWriterConfig jet_cfg;
    jet_cfg.write_kinematics_relative_to_parent = true;
    jet_cfg.double_variables = cfg::BTagLegacyDoubles;
    jet_cfg.float_variables = cfg::BTagFloats;
    jet_cfg.int_variables = cfg::BTagInts;
    if (cfg.added_ints) {
      cfg::insert_into(jet_cfg.int_variables, cfg::BTagAddedInts);
    }
    jet_cfg.char_variables = cfg::DoubleBTagAddedChars;
    jet_cfg.truth_labels = cfg::DoubleBTagTruthLabels;

    subjet_collections.emplace_back(cfg);
    auto& collection = subjet_collections.back();
    for (size_t jetn = 0; jetn < cfg.n_subjets_to_save; jetn++) {
      jet_cfg.name = "subjet_" + cfg.output_name + "_"
        + std::to_string(1 + jetn);
      collection.jet_writers.emplace_back(output, jet_cfg);
    }
    // set up track writer
    if ( cfg.n_tracks > 0 ) {
      BTagTrackWriterConfig track_cfg;
      track_cfg.float_variables = cfg::TrackFloats;
      track_cfg.uchar_variables = cfg::TrackUChars;
      track_cfg.output_size = {cfg.n_tracks};
      for (size_t jetn = 0; jetn < cfg.n_subjets_to_save; jetn++) {
        track_cfg.name = "subjet_" + cfg.output_name + "_"
          + std::to_string(1 + jetn) + "_tracks";
        collection.track_writers.emplace_back(
          new BTagTrackWriter(output, track_cfg));
      }
    }
  }

  // keep track of the events in the AOD
  Counts counts;

  // keep track of n written jets
  unsigned long long n_jets_written = 0;

  // Loop over the specified files:
  for (const std::string& file: opts.in) {

    // Open the file:
    std::unique_ptr<TFile> ifile(TFile::Open(file.c_str(), "READ"));
    if ( ! ifile.get() || ifile->IsZombie()) {
      Error( APP_NAME, "Couldn't open file: %s", file.c_str() );
      return 1;
    }
    Info( APP_NAME, "Opened file: %s", file.c_str() );

    // Connect the event object to it:
    RETURN_CHECK( APP_NAME, event.readFrom(ifile.get()) );

    // Loop over its events:
    const unsigned long long entries = event.getEntries();
    if (entries > 0) {
      event.getEntry(0);
      counts += get_counts(*ifile, event);
    }
    for (unsigned long long entry = 0; entry < entries; ++entry) {
      if (opts.n_entries && entry > opts.n_entries) break;

      // Load the event:
      if (event.getEntry(entry) < 0) {
        Error( APP_NAME, "Couldn't load entry %lld from file: %s",
               entry, file.c_str() );
        return 1;
      }

      // Print some status:
      if ( ! (entry % 500)) {
        Info( APP_NAME, "Processing entry %lld / %lld", entry, entries );
      }
      const xAOD::EventInfo* event_info = 0;
      RETURN_CHECK( APP_NAME, event.retrieve(event_info, "EventInfo") );

      const xAOD::JetContainer *raw_jets = 0;
      auto full_collection = jobcfg.jet_collection + "Jets";
      RETURN_CHECK( APP_NAME, event.retrieve(raw_jets, full_collection) );

      for (const xAOD::Jet *raw_jet : *raw_jets) { // loop over large-R jets
        std::unique_ptr<xAOD::Jet> jet(nullptr);
        xAOD::Jet* jet_ptr(nullptr);
        calib_tool.calibratedCopy(*raw_jet, jet_ptr);
        jet.reset(jet_ptr);
        if (jet->pt() < 250*GeV || std::abs(jet->eta()) > 2.0) {
          continue;
        }

        if (tagger) tagger->decorate(*jet);
        if (top_tagger) dec_top_tagger(*jet) = top_tagger->getScore(*jet);

        // get parent
        const xAOD::Jet* parent_jet = *acc_parent(*jet);
        if (!parent_jet) throw std::logic_error("no valid parent");

        fatjet_writer.write_with_parent(*jet, *parent_jet, event_info);

        // loop over subjet collections
        for(auto& collection: subjet_collections) {

          std::vector<const xAOD::Jet*> subjets;

          // depending on the jet collection we're looking at, we have
          // to grab the collection from different places
          bool use_parent = collection.cfg.get_subjets_from_parent;
          const xAOD::Jet* subjet_base = use_parent ? parent_jet : jet.get();
          auto subjet_links = collection.subjet_acc(*subjet_base);

          for (const auto& el: subjet_links) {
            const auto* sjet = dynamic_cast<const xAOD::Jet*>(*el);
            if (!sjet) throw std::logic_error("subjet is invalid");
            if (sjet->pt() > collection.cfg.min_jet_pt) {
              subjets.push_back(sjet);
            }
          }

          std::sort(subjets.begin(), subjets.end(), pt_sort);

          size_t n_writers = collection.jet_writers.size();
          bool save_tracks = collection.cfg.n_tracks > 0;
          for (size_t jet_number = 0; jet_number < n_writers; jet_number++) {
            auto& writer = collection.jet_writers.at(jet_number);
            BTagTrackWriter* track_writer = nullptr;
            if (save_tracks) {
              track_writer = collection.track_writers.at(jet_number).get();
            }
            if (jet_number < subjets.size()) {
              auto& subjet = *subjets.at(jet_number);

              // we have this in a try block because some events are
              // missing some information. We should try to fix
              // this...
              try {
                writer.write_with_parent(subjet, *jet);
              } catch (SG::ExcBadAuxVar& except) {
                BrokenEvent evt;
                evt.entry_number = entry;
                evt.event_number = event_info->eventNumber();
                evt.jet_number = jet_number;
                evt.file = file;
                evt.jet_collection = collection.cfg.input_name;
                evt.what = except.what();
                broken_events.add(evt);
                writer.write_dummy();
                // check if this is getting out of hand, die if it is
                if ( (double(entry) / entries) > opts.max_broken_fraction) {
                  double broke_frac = (
                    double(broken_events.size()) / n_jets_written);
                  if ( broke_frac > opts.max_broken_fraction) {
                    std::string broke_str = std::to_string(broke_frac);
                    throw std::runtime_error(
                      "broken event fraction " + broke_str + " to high");
                  }
                }
              } // end try block to catch corrupted events
              n_jets_written++;

              if (track_writer) {
                auto tracks = track_selector.get_tracks(subjet);
                for (const auto& track: tracks) {
                  track_augmenter.augment_with_ip(*track, subjet);
                }
                sort(tracks.begin(), tracks.end(), by_d0);
                track_writer->write(tracks, subjet);
              }
            } else {
              writer.write_dummy();
              if (track_writer) track_writer->write_dummy();
            }
          } // end loop over subjets
        } // end loop over subjet collections

      } // end jet loop

    } // end event loop
  } // end file loop

  // save count metadata
  addMetadata(output, counts);

  return 0;
}

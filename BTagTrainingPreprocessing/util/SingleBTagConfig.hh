#ifndef SINGLEBTAG_CONFIG_HH
#define SINGLEBTAG_CONFIG_HH

#include <string>
#include <vector>

enum class TrackSortOrder {ABS_D0_SIGNIFICANCE, ABS_D0, D0_SIGNIFICANCE};

struct SingleBTagConfig {
  std::string jet_collection;
  std::string jet_calib_file;
  std::string cal_seq;
  std::string cal_area;
  bool do_calibration;
  float jvt_cut;
  std::string iprnn_file_path;
  size_t n_tracks_to_save;
  TrackSortOrder track_sort_order;
};

SingleBTagConfig get_singlebtag_config(const std::string& config_file_name);

#endif
